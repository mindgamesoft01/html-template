package com.mindgame.htmltemplate;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;

/**
 * Created by lenovo on 16-Apr-17.
 */
public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
    private String []  loadingimags;
    private Context context;
    AdClass adClass=new AdClass();
    singleton_images sc=singleton_images.getInstance();
    InterstitialAd interstitialAd;
    String AD_UNIT_ID_full_page ="ca-app-pub-4951445087103663/6977921957";

    InterstitialAd interstitialAd1;
    String TAG="AD STATUS";
    String AD_UNIT_ID_full_page1 ="ca-app-pub-4951445087103663/6977921957";
    NetworkStatusCheck NC=NetworkStatusCheck.getInstance();
    Utilityadds uds;
    ProgressDialog pd = new ProgressDialog(context.getApplicationContext());
    final AdRequest adRequest = new AdRequest.Builder().build();


    String app_status = sc.app_mode();
    public DataAdapter(Context context, String []  loading_imags) {
        this.context = context;
        this.loadingimags = loading_imags;

    }

    @Override
    public DataAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_layout, viewGroup, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(final ViewHolder viewHolder, final int i) {

        interstitialAd = new InterstitialAd(context.getApplicationContext());
        interstitialAd1 = new InterstitialAd(context.getApplicationContext());


        interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

        final AdRequest adRequest1 = new AdRequest.Builder().build();
        interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Add Loading ...");
        pd.setIndeterminate(true);
        uds=new Utilityadds();

       /* Glide.with(context).load(loadingimags.get(i))
                .thumbnail(0.5f)
                .crossFade()
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(viewHolder.img);*/
       // viewHolder.img.setImageURI(loadingimags.get(i));
   //     Picasso.with(context).load(loadingimags.get(i).getImage_url()).resize(120, 60).into(viewHolder.img);
      //  Picasso.with(context).load(loadingimags.get(i).getImage_url()).into(viewHolder.img);

        //Picasso.with(context).load(loadingimags.get(i).getImage_url()).resize(120, 60).into(viewHolder.img);
        //Glide.with(context).load(loadingimags[i]).into(viewHolder.img);
        viewHolder.img.setText(loadingimags[i]);

    }

    @Override
    public int getItemCount() {
        return loadingimags.length;
    }


    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener{

        TextView img;
        public ViewHolder(final View view) {
            super(view);

            img= (TextView) view.findViewById(R.id.img1);


//            img.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//
//                    //new AdListner().AdMobInterstitial(v.getContext(),ImageviewActivity.class,getPosition());
//
//
//                    new Utilityadds().AdMobInterstitia_context(context.getApplicationContext());
//
//                    Intent i=new Intent(v.getContext(),Webview_Activity.class);
//                    i.putExtra("images",loadingimags);
//                    i.putExtra("POS",getAdapterPosition());
//                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                    context.startActivity(i);
//
//                    //  context.startActivity(new Intent(v.getContext(), ImageviewActivity.class).putExtra("POS", getPosition()).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK));
//                }
//            });
            img.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    //interstitialAd.setAdUnitId(interstitial0);

                    //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                    if (NC.isOnline(context.getApplicationContext()) == Boolean.FALSE) {
                        Intent i=new Intent(v.getContext(),Webview_Activity.class);
                  i.putExtra("images",loadingimags);
                  i.putExtra("POS",getAdapterPosition());
                   i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    context.startActivity(i);
                        pd.dismiss();
                    }
                    if (NC.isOnline(context.getApplicationContext()) == Boolean.TRUE) {

                        Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                        pd.show();
                        Log.e(TAG, ad1_status.toString());
                        if (ad1_status == Boolean.FALSE) {
                            Intent i=new Intent(v.getContext(),Webview_Activity.class);
                            i.putExtra("images",loadingimags);
                            i.putExtra("POS",getAdapterPosition());
                            i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            context.startActivity(i);
                            pd.dismiss();


                        }


                        if (ad1_status == Boolean.TRUE && NC.isOnline(context.getApplicationContext()) == Boolean.TRUE) {
                            interstitialAd.setAdListener(new AdListener() {
                                @Override
                                public void onAdClosed() {
                                    super.onAdClosed();
                                    Toast.makeText(context.getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                    Intent i=new Intent(context.getApplicationContext(),Webview_Activity.class);
                                    i.putExtra("images",loadingimags);
                                    i.putExtra("POS",getAdapterPosition());
                                    i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(i);
                                    pd.dismiss();


                                }

                                @Override
                                public void onAdFailedToLoad(int i) {
                                    super.onAdFailedToLoad(i);

                                    if (NC.isOnline(context.getApplicationContext()) == Boolean.FALSE) {
                                        //cd.show();
                                        pd.dismiss();
                                    }
                                    Intent j=new Intent(context.getApplicationContext(),Webview_Activity.class);
                                    j.putExtra("images",loadingimags);
                                    j.putExtra("POS",getAdapterPosition());
                                    j.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                    context.startActivity(j);
                                    pd.dismiss();

                                    //set the last loaded timestamp even if the ad load fails
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                }

                                @Override
                                public void onAdLeftApplication() {
                                    super.onAdLeftApplication();
                                }

                                @Override
                                public void onAdOpened() {
                                    super.onAdOpened();
                                }

                                @Override
                                public void onAdLoaded() {
                                    super.onAdLoaded();
                                    //set the last loaded timestamp
                                    singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    interstitialAd.show();
                                    pd.dismiss();

                                }
                            });
                            interstitialAd.loadAd(adRequest);
                        }
                    }
                }
            });
        }

        @Override
        public void onClick(View v) {

        }
    }
}