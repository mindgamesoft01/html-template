package com.mindgame.htmltemplate;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.widget.LinearLayout;


/**
 * Created by kalyani on 9/5/2017.
 */

public class Webview_Activity extends Activity {
    String []html={"file:///android_asset/index1.html","file:///android_asset/index2.html","file:///android_asset/index3.html","file:///android_asset/index4.html",
            "file:///android_asset/index5.html","file:///android_asset/index6.html","file:///android_asset/index7.html","file:///android_asset/index8.html","file:///android_asset/index9.html",
            "file:///android_asset/index10.html","file:///android_asset/index11.html","file:///android_asset/index12.html","file:///android_asset/index13.html"};
    WebView webView;
    int position;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    int scale;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);
        webView=(WebView)findViewById(R.id.webview);
        WebSettings setting = webView.getSettings();

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);





        setting.setJavaScriptEnabled(true);
        setting.setBuiltInZoomControls(true);
        setting.setSupportZoom(true);
        webView.setInitialScale(0);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        this.webView.getSettings().setDefaultZoom(WebSettings.ZoomDensity.FAR);
        webView.setScrollbarFadingEnabled(false);
        Bundle b= getIntent().getExtras();
        position=b.getInt("flag");
        webView.loadUrl(html[position]);


    }
}
