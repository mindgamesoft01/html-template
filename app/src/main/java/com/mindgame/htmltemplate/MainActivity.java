package com.mindgame.htmltemplate;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;

import java.util.Date;

public class MainActivity extends Activity {
    TextView textView1,textView2,textView3,textView4,textView5,textView6,textView7,textView8,textView9,textView10;
    LinearLayout layout_one,layout_two,layout_three,layout_four,layout_five,layout_six,layout_seven,layout_eight,layout_nine,layout_ten;

    String val;
    TextView heading;
    LinearLayout layout, strip, layout1, strip1;
    AdClass ad = new AdClass();
    Button btn1,btn2,btn3,btn4,btn5,btn6,btn7,btn8,btn9,btn10,btn11,btn12,btn13;
    singleton_images sc=singleton_images.getInstance();
    InterstitialAd interstitialAd;
    String AD_UNIT_ID_full_page ="ca-app-pub-4951445087103663/9343069725";
    InterstitialAd interstitialAd1;
    String TAG="AD STATUS";
    String AD_UNIT_ID_full_page1 ="ca-app-pub-4951445087103663/9343069725";

    Boolean connected;

    NetworkStatusCheck NC=NetworkStatusCheck.getInstance();
    TextView terms_privacy;

    ScrollView scrollView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
//        Bundle b=getIntent().getExtras();
//        val=b.getString("flag");

        layout = (LinearLayout) findViewById(R.id.admob);
        strip = ad.layout_strip(this);
        layout.addView(strip);
        ad.AdMobBanner1(this);

        final ProgressDialog pd = new ProgressDialog(this);
        pd.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        pd.setMessage("Please wait ..." + "Add Loading ...");
        pd.setIndeterminate(true);
        pd.setCancelable(false);
        interstitialAd = new InterstitialAd(this);
        interstitialAd1 = new InterstitialAd(this);

        final AdRequest adRequest = new AdRequest.Builder().build();
        interstitialAd.setAdUnitId(AD_UNIT_ID_full_page);

        final AdRequest adRequest1 = new AdRequest.Builder().build();
        interstitialAd1.setAdUnitId(AD_UNIT_ID_full_page1);
        String app_status = sc.app_mode();

        initViews();
        if (app_status.equals("TEST")) {



        }
        if (app_status.equals("PROD")) {


                layout_one.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",1);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",1);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",1);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",1);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });
                layout_two.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                       // Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",2);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",2);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",2);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",2);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });
                layout_three.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",3);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",3);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",3);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",3);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });

                layout_four.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",4);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",4);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",4);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",4);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });

                layout_five.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",5);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",5);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",5);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",5);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });

                layout_six.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",6);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",6);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",6);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",6);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });

                layout_seven.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",7);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",7);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",7);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",7);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });

                layout_eight.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",8);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",8);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",8);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",8);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });

                layout_nine.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",9);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",9);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",9);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",9);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });

                layout_ten.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {


                        //interstitialAd.setAdUnitId(interstitial0);

                        //Log.d("NETWORK", "Layout1 Status is:" + NC.network_status.toString());
                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                            Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                            Bundle b=new Bundle();
                            b.putInt("flag",10);
                            i.putExtras(b);
                            startActivity(i);
                            pd.dismiss();
                        }
                        if (NC.isOnline(getApplicationContext()) == Boolean.TRUE) {

                            Boolean ad1_status = singleton_images.getInstance().get_ad_status(1);
                            pd.show();
                            Log.e(TAG, ad1_status.toString());
                            if (ad1_status == Boolean.FALSE) {
                                Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                Bundle b=new Bundle();
                                b.putInt("flag",10);
                                i.putExtras(b);
                                startActivity(i);
                                pd.dismiss();


                            }


                            if (ad1_status == Boolean.TRUE && NC.isOnline(getApplicationContext()) == Boolean.TRUE) {
                                interstitialAd.setAdListener(new AdListener() {
                                    @Override
                                    public void onAdClosed() {
                                        super.onAdClosed();
                                        Toast.makeText(getApplicationContext(), "Thanks for watching ad...", Toast.LENGTH_LONG).show();
                                        Intent i = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",10);
                                        i.putExtras(b);
                                        startActivity(i);
                                        pd.dismiss();


                                    }

                                    @Override
                                    public void onAdFailedToLoad(int i) {
                                        super.onAdFailedToLoad(i);

                                        if (NC.isOnline(getApplicationContext()) == Boolean.FALSE) {
                                            //cd.show();
                                            pd.dismiss();
                                        }
                                        Intent j = new Intent(getApplicationContext(), Webview_Activity.class);
                                        Bundle b=new Bundle();
                                        b.putInt("flag",10);
                                        j.putExtras(b);
                                        startActivity(j);
                                        pd.dismiss();

                                        //set the last loaded timestamp even if the ad load fails
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                    }

                                    @Override
                                    public void onAdLeftApplication() {
                                        super.onAdLeftApplication();
                                    }

                                    @Override
                                    public void onAdOpened() {
                                        super.onAdOpened();
                                    }

                                    @Override
                                    public void onAdLoaded() {
                                        super.onAdLoaded();
                                        //set the last loaded timestamp
                                        singleton_images.getInstance().setLastIntestitial_loadded(new Date().getTime());

                                        interstitialAd.show();
                                        pd.dismiss();

                                    }
                                });
                                interstitialAd.loadAd(adRequest);
                            }
                        }
                    }
                });




            }
        }


    private void initViews() {
        textView1=(TextView)findViewById(R.id.text1);
        textView2=(TextView)findViewById(R.id.text2);
        textView3=(TextView)findViewById(R.id.text3);
        textView4=(TextView)findViewById(R.id.text4);
        textView5=(TextView)findViewById(R.id.text5);
        textView6=(TextView)findViewById(R.id.text6);
        textView7=(TextView)findViewById(R.id.text7);
        textView8=(TextView)findViewById(R.id.text8);
        textView9=(TextView)findViewById(R.id.text9);
        textView10=(TextView)findViewById(R.id.text10);

        layout_one=(LinearLayout)findViewById(R.id.layout_one);
        layout_two=(LinearLayout)findViewById(R.id.layout_two);
        layout_three=(LinearLayout)findViewById(R.id.layout_three);
        layout_four=(LinearLayout)findViewById(R.id.layout_four);
        layout_five=(LinearLayout)findViewById(R.id.layout_five);
        layout_six=(LinearLayout)findViewById(R.id.layout_six);
        layout_seven=(LinearLayout)findViewById(R.id.layout_seven);
        layout_eight=(LinearLayout)findViewById(R.id.layout_eight);
        layout_nine=(LinearLayout)findViewById(R.id.layout_nine);
        layout_ten=(LinearLayout)findViewById(R.id.layout_ten);
        textView1.setText(R.string.one);
        textView2.setText(R.string.two);
        textView3.setText(R.string.three);
        textView4.setText(R.string.four);
        textView5.setText(R.string.five);
        textView6.setText(R.string.six);
        textView7.setText(R.string.seven);
        textView8.setText(R.string.eight);
        textView9.setText(R.string.nine);
        textView10.setText(R.string.ten);




    }

}
